const express = require('express');

const{
    createDep,
    getDeps
} = require('../controllers/departements');

const router = express.Router();

router
    .route('/')
    .get(getDeps).post(createDep);

module.exports = router;