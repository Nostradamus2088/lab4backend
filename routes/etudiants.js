const express = require('express');

const{
    createStudent,
    getStudents,
    getStudent,
    delStudent,
    updateStudent
} = require('../controllers/etudiants');

const router = express.Router();

router
    .route('/')
    .get(getStudents)
    .post(createStudent);

    router.route('/:id')
        .get(getStudent)
        .delete(delStudent)
        .put(updateStudent);

module.exports = router;