const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require("../middleware/async");
const departement = require('../models/dep');


//@desc Create new Departement
//@route POST /Departement
//@access Private
exports.createDep = asyncHandler (async(req,res, next)=>{
    const depart = await(departement.create(req.body));

    res.status(201).json({
        success:true,
        data: depart
    });
});

//@desc GET all Departement
//@route GET /Departement
//@access Public
exports.getDeps = asyncHandler (async(req,res, next)=>{
   
    const depart = await departement.find();     
   
    res.status(201).json({
       success:true,
       count: depart.length,
       data: depart
   }); 
});
