const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require("../middleware/async");
const etudiant = require('../models/etudiant');




//@desc Create new etudiant
//@route POST /etudiant
//@access Private
exports.createStudent = asyncHandler (async(req,res, next)=>{
    const student = await(etudiant.create(req.body));

    res.status(201).json({
        success:true,
        data: student
    });
});

//@desc GET all etudiant
//@route GET /etudiant
//@access Public
exports.getStudents = asyncHandler (async(req,res, next)=>{
   
     const students = await etudiant.find();     
    
     res.status(201).json({
        success:true,
        count: students.length,
        data: students
    }); 
});

//@desc GET an etudiant
//@route GET /etudiant/:id
//@access Public
exports.getStudent = asyncHandler (async(req,res, next)=>{
    const students = await etudiant.findById(req.params.id);
    if(!students){
        return next(
            new ErrorResponse(`Student not found with the id of ${req.params.id}`,404)
        );
    }

    res.status(201).json({
        success:true,
        data: students
    });
});

//@desc DELETE an etudiant
//@route DELETE /etudiant/:id
//@access PRIVATE
exports.delStudent = asyncHandler (async(req,res, next)=>{
    const students = await etudiant.findById(req.params.id);
    if(!students){
        return next(
            new ErrorResponse(`Student not found with the id of ${req.params.id}`,404)
        );
    }
    students.remove();

    res.status(201).json({
        success:true,
        data: {}
    });
});



//@desc Update an etudiant
//@route PUT /etudiant/:id
//@access PRIVATE
exports.updateStudent = asyncHandler (async(req,res, next)=>{
    const students = await etudiant.findByIdAndUpdate(req.params.id, req.body,{
        new: true,
        runValidators: true
    });
    if(!students){
        return next(
            new ErrorResponse(`Student not found with the id of ${req.params.id}`,404)
        );
    }
    res.status(201).json({
        success:true,
        data: students
    });
});

