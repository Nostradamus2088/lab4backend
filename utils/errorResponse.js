class ErrorResponse extends Error {
    constructor(message, statusCode) {   //le constructeur va passer un message et un status code
        super(message);
        this.statusCode = statusCode;
    }
}
module.exports = ErrorResponse;